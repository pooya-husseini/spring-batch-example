package com.navaco.reader;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class StringReader implements ItemReader<String> {
    private int a = 0;

    @Override
    public String read() throws Exception {
        if (a >= 100) {
            return null;
        }
        return "Hi" + a++;
    }
}
