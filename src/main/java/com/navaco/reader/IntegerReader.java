package com.navaco.reader;

import org.springframework.batch.item.ItemReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class IntegerReader implements ItemReader<Integer> {

    private Iterator<Integer> integerIterable;


    public IntegerReader() {
        List<Integer> integers = new ArrayList<Integer>();
        for (int i = 0; i < 100000; i++) {
            integers.add(i);
        }

        integerIterable = integers.iterator();
    }

    @Override
    public Integer read() throws Exception {
        if (integerIterable.hasNext()) {
            return integerIterable.next();
        }
        return null;
    }
}
