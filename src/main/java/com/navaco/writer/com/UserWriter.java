package com.navaco.writer.com;

import com.navaco.User;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class UserWriter implements ItemWriter<User> {
    @Override
    public void write(List<? extends User> items) throws Exception {
        System.out.println(items);
    }
}
