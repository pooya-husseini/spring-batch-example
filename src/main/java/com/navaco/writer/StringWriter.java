package com.navaco.writer;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class StringWriter implements ItemWriter<String> {
    @Override
    public void write(List<? extends String> list) throws Exception {
        System.out.println(list);
    }
}
