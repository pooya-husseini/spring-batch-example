package com.navaco.writer;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class IntegerWriter implements ItemWriter<Integer> {
    @Override
    public void write(List<? extends Integer> list) throws Exception {
        System.out.println(list);
    }
}
