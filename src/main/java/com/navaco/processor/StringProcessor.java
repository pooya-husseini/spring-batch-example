package com.navaco.processor;

import org.springframework.batch.item.ItemProcessor;

/**
 * @author pooya
 *         date : 1/26/16
 */
public class StringProcessor implements ItemProcessor<String, String> {
    @Override
    public String process(String integer) throws Exception {
        return integer + 1;
    }
}
