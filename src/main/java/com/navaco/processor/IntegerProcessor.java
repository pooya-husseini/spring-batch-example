package com.navaco.processor;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 * @author pooya
 *         date : 1/26/16
 */

@Scope("step")
public class IntegerProcessor implements ItemProcessor<Integer, Integer> {

    @Autowired
    private ExecutionContext executionContext;

    @Override
    public Integer process(Integer integer) throws Exception {
        System.out.println(executionContext.getString("family"));
        return integer + 1;
    }
}
