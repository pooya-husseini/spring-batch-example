package com.navaco;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        App obj = new App();
        obj.run();
    }

    private void run() {
        String[] springConfig = {"spring/batch/jobs/job-partitioner.xml"};
        ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("partitionJob");
        Job itemJob= (Job) context.getBean("itemJob");
        try {
            //JobParameters param = new JobParametersBuilder().addString("age", "20").toJobParameters();
            JobExecution jobExecution = jobLauncher.run(job, new JobParameters());
//            JobExecution jobExecution = jobLauncher.run(itemJob, new JobParameters());
            System.out.println("Exit Status : " + jobExecution.getStatus());
            System.out.println("Exit Status : " + jobExecution.getAllFailureExceptions());

            long l = jobExecution.getEndTime().getTime() - jobExecution.getStartTime().getTime();
            System.out.println("Execution time = "+l);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }
}